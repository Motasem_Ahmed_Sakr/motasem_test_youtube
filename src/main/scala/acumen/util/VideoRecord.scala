package acumen.util

import java.awt.Color
import java.awt.image.BufferedImage
import java.io.{BufferedInputStream, BufferedReader, FileInputStream, IOException, InputStreamReader}
import java.util.concurrent.ConcurrentLinkedQueue

import acumen.ui.App
import acumen.util.VideoRecord.VideoQuality
import com.google.api.client.auth.oauth2.{Credential, TokenResponseException}
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver
import com.google.api.client.googleapis.auth.oauth2.{GoogleAuthorizationCodeFlow, GoogleClientSecrets}
import com.google.api.client.googleapis.extensions.java6.auth.oauth2.GooglePromptReceiver
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.googleapis.media.{MediaHttpUploader, MediaHttpUploaderProgressListener}
import com.google.api.client.http.InputStreamContent
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.youtube.YouTube
import com.google.api.services.youtube.model.{Video, VideoSnippet, VideoStatus}
import com.threed.jpct.FrameBuffer
import org.jcodec.api.SequenceEncoder
import org.jcodec.common.model.{ColorSpace, Picture}

import scala.reflect.io.File

/**
  * Created by Kalashnikov Vlad on 28.11.2016.
  */
class VideoRecord(var quality: VideoQuality) {

  if ((quality.width <= 2) || (quality.height <= 2)) {
    quality = VideoRecord.Quality480
  }

  val width: Int = quality.width - (quality.width % 2)
  val height: Int = quality.height - (quality.height % 2)

  private val _tempVideoFile = File.makeTemp("acumen_video_", ".mp4")
  private val _recordThread = new Thread() {
    private val _waitTime = 50
    private var _encoder: Option[SequenceEncoder] = None
    private val _queueRecord = new ConcurrentLinkedQueue[BufferedImage]

    private var _switchedOff = false

    private var _inEncode = false

    def inProgress: Boolean = _inEncode || !_queueRecord.isEmpty

    def isRunning: Boolean = inProgress || _encoder.isDefined

    private def pictureFromBufferedImage(im: BufferedImage): Picture = {
      val resultPicture = Picture.create(width, height, ColorSpace.RGB)
      val planeData = resultPicture.getPlaneData(0)

      var index = 0
      for (y <- 0 until height) {
        for (x <- 0 until width) {
          val color = new Color(im.getRGB(x, y), false)
          planeData(index) = color.getRed
          index += 1
          planeData(index) = color.getGreen
          index += 1
          planeData(index) = color.getBlue
          index += 1
        }
      }

      resultPicture
    }

    def startRecord(videoFile: File): Unit = {
      if (!isRunning) {
        _encoder = Some(new SequenceEncoder(videoFile.jfile))
        _switchedOff = false
        super.start()
      }
    }

    def stopRecord(): Unit = {
      if (!_switchedOff && isRunning) {
        _switchedOff = true
        val doubleWaitTime = _waitTime * 2
        while (inProgress) {
          try {
            Thread.sleep(doubleWaitTime)
          } catch {
            case e: InterruptedException => e.printStackTrace()
          }
        }
        _encoder.get.finish()
        _encoder = None
      }
    }

    def putShot(shot: BufferedImage): Unit = {
      _queueRecord.add(shot)
    }

    override def run() {
      while (isRunning) {
        if (_queueRecord.isEmpty) {
          Thread.sleep(_waitTime)
        } else {
          try {
            _inEncode = true
            _encoder.get.encodeNativeFrame(pictureFromBufferedImage(_queueRecord.remove()))
            _inEncode = false
          } catch {
            case e: IOException => e.printStackTrace()
          }
        }
      }
    }
  }
  private var _videoTime: Double = 0

  _recordThread.startRecord(_tempVideoFile)

  def tempVideoFile: File = _tempVideoFile

  def videoTime: Double = _videoTime

  /** Add new shot to recording video */
  def addFormShot(): Unit = {
    _videoTime += 0.04
    val imageBuffer = new FrameBuffer(width, height, FrameBuffer.SAMPLINGMODE_OGSS)
    App.ui.threeDtab.threeDView.paintToBuffer(imageBuffer)
    val im = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
    val g2d = im.createGraphics()
    imageBuffer.display(g2d)
    g2d.dispose()

    _recordThread.putShot(im)
  }

  /** Stop record and save video as file
    *
    * @param file Save video as this file
    * @return Success result
    */
  def saveVideoAsFile(file: File): Boolean = {
    saveVideo()

    try {
      file.outputStream().getChannel.transferFrom(
        _tempVideoFile.inputStream().getChannel, 0, Long.MaxValue)
      true
    } catch {
      case _: java.io.IOException => false
    }
  }

  /** Stop record */
  def saveVideo(): Unit = _recordThread.stopRecord()

  def removeVideo(): Boolean = _tempVideoFile.exists && _tempVideoFile.delete()
}

object VideoRecord {

  private var _youtube: Option[YouTube] = None

  def youtubeInstance(youtubeUser: Option[youtubeUserInfo]): YouTube = {
    if (_youtube.isEmpty) {
      youtubeReauthorization(youtubeUser)
    }

    _youtube.get
  }

  def youtubeReauthorization(youtubeUser: Option[youtubeUserInfo]): YouTube = {
    var credential: Option[Credential] = None

    val secretReader = new InputStreamReader(VideoRecord.getClass.getResourceAsStream("/client_secrets.json"), "UTF-8")
    val flow = new GoogleAuthorizationCodeFlow.Builder(
      GoogleNetHttpTransport.newTrustedTransport(),
      new JacksonFactory(),
      GoogleClientSecrets.load(new JacksonFactory(), secretReader),
      java.util.Collections.singleton("https://www.googleapis.com/auth/youtube.upload")
    )
      .setAccessType("offline")
      .setApprovalPrompt("force")
      .build()
    val userID = "user"

    if (youtubeUser.isDefined) {
      val jsCode = "var page = require('webpage').create();\n" +
        "page.settings.userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36';\n" +
        "page.viewportSize = {\n" +
        "  width: 1280,\n" +
        "  height: 720\n" +
        "};\n" +
        "\n" +
        "page.settings.resourceTimeout = 10000; // 10 seconds\n" +
        "var savePngFiles=false;\n" +
        "\n" +
        "var system = require('system');\n" +
        "var args = system.args;\n" +
        "\n" +
        "var email='email@domain.com';\n" +
        "var password='password';\n" +
        "var url='https://accounts.google.com/o/oauth2/auth?'\n" +
        "  + 'scope=https://www.googleapis.com/auth/youtube.upload'\n" +
        "  + '&response_type=code'\n" +
        "  + '&redirect_uri=urn:ietf:wg:oauth:2.0:oob'\n" +
        "  + '&access_type=offline'\n" +
        "  + '&approval_prompt=force';\n" +
        "\n" +
        "if(args.length === 4) {\n" +
        "  email = args[1];\n" +
        "  password = args[2];\n" +
        "  url = url + '&client_id=' + args[3];\n" +
        "} else {\n" +
        "  console.log('Required arguments are not found');\n" +
        "  phantom.exit(1);\n" +
        "}\n" +
        "\n" +
        "var fillInput = function(str) {\n" +
        "  for(var i=0; i<str.length; i++) {\n" +
        "    page.sendEvent('keypress', str.charAt(i));\n" +
        "  }\n" +
        "  page.sendEvent('keypress', page.event.key.Enter);\n" +
        "};\n" +
        "\n" +
        "var saveRenderedPng=function(fileName) {\n" +
        "  if (savePngFiles == true) {\n" +
        "    page.render(fileName);\n" +
        "  }\n" +
        "};\n" +
        "\n" +
        "page.open(url, function (status) {\n" +
        "  page.includeJs(\"http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js\", function() {\n" +
        "    //console.log('Set email');\n" +
        "    fillInput(email);\n" +
        "\n" +
        "    setTimeout(function(){\n" +
        "      saveRenderedPng('1.png');\n" +
        "      //console.log('Set password');\n" +
        "      fillInput(password);\n" +
        "\n" +
        "      setTimeout(function(){\n" +
        "        saveRenderedPng('2.png');\n" +
        "        page.sendEvent('keypress', page.event.key.Enter);\n" +
        "\n" +
        "        setTimeout(function(){\n" +
        "          saveRenderedPng('3.png');\n" +
        "          //console.log('Approve scopes');\n" +
        "          page.evaluate(function() {\n" +
        "            var a = document.getElementById(\"submit_approve_access\");\n" +
        "            var e = document.createEvent('MouseEvents');\n" +
        "            e.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);\n" +
        "            a.dispatchEvent(e);\n" +
        "          });\n" +
        "\n" +
        "          setTimeout(function(){\n" +
        "            saveRenderedPng('4.png');\n" +
        "            var code=page.evaluate(function(){\n" +
        "              return document.getElementById('code').value;\n" +
        "            });\n" +
        "            console.log(code);\n" +
        "\n" +
        "            phantom.exit();\n" +
        "          }, 2000);\n" +
        "        }, 5000);\n" +
        "      }, 2000);\n" +
        "    }, 2000);\n" +
        "  });\n" +
        "});"

      val phantomCommandFile = File.makeTemp("get_oauth_code", ".js")
      phantomCommandFile.writeAll(jsCode)

      try {
        val process = Runtime.getRuntime.exec(s"phantomjs ${phantomCommandFile.path} ${youtubeUser.get.email} ${youtubeUser.get.password} ${flow.getClientId}")
        process.waitFor()
        val codeFromPhantom = new BufferedReader(new InputStreamReader(process.getInputStream)).readLine()

        credential = Some(flow.createAndStoreCredential(flow.newTokenRequest(codeFromPhantom).setRedirectUri("urn:ietf:wg:oauth:2.0:oob").execute(), userID))
      } catch {
        case _@(_: TokenResponseException | _: java.io.IOException) => credential = Some(new AuthorizationCodeInstalledApp(flow, new GooglePromptReceiver()).authorize(userID))
      }

      phantomCommandFile.delete()
    }

    if (credential.isEmpty || (credential.isDefined && credential.get == null)) {
      credential = Some(new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize(userID))
    }

    _youtube = Some(new YouTube.Builder(GoogleNetHttpTransport.newTrustedTransport(), new JacksonFactory(), credential.get).build())

    _youtube.get
  }

  /**
    * Method for upload some video on YouTube
    *
    * @return Url to video as String
    */
  def uploadOnYoutube(name: String, file: File, youtube: YouTube, logger: Option[UploadLogger]): Option[String] = {
    var result: Option[String] = None
    if (file.exists && (file.length > 0)) {
      // Extra information about video
      val videoObjectDefiningMetadata = new Video()
      // Public video status
      val status = new VideoStatus()
      status.setPrivacyStatus("public")
      videoObjectDefiningMetadata.setStatus(status)

      val snippet = new VideoSnippet()
      snippet.setTitle(name)
      videoObjectDefiningMetadata.setSnippet(snippet)

      // Work with file
      val mediaContent = new InputStreamContent("video/*",
        new BufferedInputStream(new FileInputStream(file.jfile)))
      mediaContent.setLength(file.length)

      // Insert the video:
      // The first - API request is setting and which information the API deviceResponse should return
      // The second - metadata about the new video
      // The third - video content
      val videoInsert = youtube.videos()
        .insert("snippet,statistics,status", videoObjectDefiningMetadata, mediaContent)

      // Set the upload type and add an event listener
      val uploader = videoInsert.getMediaHttpUploader
      //if true media content will be uploaded in a single request
      // if false will use the resumable media upload protocol
      uploader.setDirectUploadEnabled(false)

      // Progress status
      val progressListener = new MediaHttpUploaderProgressListener {
        override def progressChanged(uploader: MediaHttpUploader): Unit = {
          if (logger.isDefined) {
            uploader.getUploadState match {
              case MediaHttpUploader.UploadState.INITIATION_STARTED => logger.get.status = MediaHttpUploader.UploadState.INITIATION_STARTED
              case MediaHttpUploader.UploadState.INITIATION_COMPLETE => logger.get.status = MediaHttpUploader.UploadState.INITIATION_COMPLETE
              case MediaHttpUploader.UploadState.MEDIA_IN_PROGRESS => logger.get.progress(uploader.getProgress)
              case MediaHttpUploader.UploadState.MEDIA_COMPLETE => logger.get.status = MediaHttpUploader.UploadState.MEDIA_COMPLETE
              case MediaHttpUploader.UploadState.NOT_STARTED => logger.get.status = MediaHttpUploader.UploadState.NOT_STARTED
            }
          }
        }
      }
      uploader.setProgressListener(progressListener)

      val returnedVideo = videoInsert.execute()

      result = Some(s"https://www.youtube.com/watch?v=${returnedVideo.getId}")
    }

    result
  }

  abstract class VideoQuality {
    def height: Int

    def width: Int

    def isCorrectQuality: Boolean = {
      (height > 2) && (width > 2)
    }
  }

  abstract class UploadLogger {
    var progressListener: Double
    private var _status: Option[MediaHttpUploader.UploadState] = None

    def status: Option[MediaHttpUploader.UploadState] = _status

    def status_=(value: MediaHttpUploader.UploadState): Unit = _status = Some(value)

    def progress(value: Double): Unit =
      if ((value >= 0) && (value <= 1)) {
        progressListener = value
        Console.out.println(s"YouTube Upload: ${value.toString}")
      }
  }

  case class youtubeUserInfo(email: String, password: String)

  object Quality720 extends VideoQuality {
    val height = 720
    val width = 1280
  }

  object Quality480 extends VideoQuality {
    val height = 480
    val width = 720
  }

}